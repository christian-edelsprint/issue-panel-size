# Instructions

1. Run `forge deploy`.
2. Run `forge install`.
3. Open the `issue-panel-size` issue panel on an issue.
4. There's too much whitespace below the div because the iframe is too large.
